export function createSkuList(all) {
  // const names = ['iphoneX', 'iphoneXs']
  // const colors = ['黑色', '白色']
  // const sizes = ['64G', '128G']
  // const c = ['2', '3']
  // args.forEach(item => {
  //   console.log(item)
  // })
  //
  // const all = [names, colors, sizes, c]
  const combine = function(all) {
    if (all.length < 2) {
      return all[0] || []
    }
    return all.reduce((pre, cur) => {
      const res = []
      pre.forEach(p => {
        cur.forEach(c => {
          const t = [].concat(Array.isArray(p) ? p : [p])
          t.push(c)
          res.push(t)
        })
      })
      return res
    })
  }

  console.log(combine(all))
}
