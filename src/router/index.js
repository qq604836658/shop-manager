import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
 roles: ['admin','editor']    control the page roles (you can set multiple roles)
 title: 'title'               the name show in sidebar and breadcrumb (recommend set)
 icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
 breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
 activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
 }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path: '/setting',
    component: Layout,
    name: 'setting',
    meta: {
      title: '设置',
      icon: 'el-icon-setting'
    },
    children: [
      {
        path: 'userManager',
        component: () => import('@/views/setting/user'),
        name: 'userManager',
        meta: { title: '用户管理', icon: 'el-icon-user' },
        children: [
          {
            path: 'user',
            component: () => import('@/views/setting/user/user/userList.vue'),
            name: 'user',
            meta: { title: '用户管理', icon: 'el-icon-s-operation' }
          },
          {
            path: 'role',
            component: () => import('@/views/setting/user/role/roleList.vue'),
            name: 'role',
            meta: { title: '角色管理', icon: 'el-icon-s-operation' }
          },
          {
            path: 'menu',
            component: () => import('@/views/setting/user/menu/menuList.vue'),
            name: 'menu',
            meta: { title: '菜单管理', icon: 'el-icon-s-operation' }
          }
        ]
      },
      {
        path: 'system',
        component: () => import('@/views/setting/system'),
        name: 'system',
        meta: { title: '系统设置', icon: 'el-icon-s-tools' },
        children: [
          {
            path: 'systemSetting',
            component: () => import('@/views/setting/system/systemSetting/systemSetting.vue'),
            name: 'systemSetting',
            meta: { title: '系统设置', icon: 'el-icon-s-operation' }
          },
          {
            path: 'region',
            component: () => import('@/views/setting/system/region/region.vue'),
            name: 'region',
            meta: { title: '行政地区', icon: 'el-icon-s-operation' }
          },
          {
            path: 'logisticsCompany',
            component: () => import('@/views/setting/system/logisticsCompany/logisticsCompany.vue'),
            name: 'logisticsCompany',
            meta: { title: '物流公司', icon: 'el-icon-s-operation' }
          }

        ]
      }
    ]
  },
  {
    path: '/product',
    component: Layout,
    name: 'product',
    meta: {
      title: '商品',
      icon: 'el-icon-goods'
    },
    children: [
      {
        path: 'productManager',
        component: () => import('@/views/product/productManager'),
        name: 'productManager',
        meta: { title: '商品管理', icon: 'el-icon-s-goods' },
        children: [
          {
            path: 'productList',
            component: () => import('@/views/product/productManager/productList.vue'),
            name: 'productList',
            meta: { title: '商品列表', icon: 'el-icon-s-operation' }
          },
          {
            path: 'productReviewList',
            component: () => import('@/views/product/productManager/productReviewList.vue'),
            name: 'productReviewList',
            meta: { title: '商品审核', icon: 'el-icon-s-operation' }
          },
          {
            path: 'productDetails',
            component: () => import('@/views/product/productManager/productDetails.vue'),
            name: 'productDetails',
            hidden: 1,
            meta: { title: '商品详情', icon: 'el-icon-menu' }
          },
          {
            path: 'productReviewDetails',
            component: () => import('@/views/product/productManager/productReviewDetails.vue'),
            name: 'productReviewDetails',
            hidden: 1,
            meta: { title: '商品审核详情', icon: 'el-icon-menu' }
          }
        ]
      },
      {
        path: 'productAttributes',
        component: () => import('@/views/product/productAttributes'),
        name: 'productAttributes',
        meta: { title: '商品属性', icon: 'el-icon-s-shop' },
        children: [
          {
            path: 'productClass',
            component: () => import('@/views/product/productAttributes/productClass'),
            name: 'productClass',
            meta: { title: '商品分类', icon: 'el-icon-s-operation' }
          },
          {
            path: 'productBrand',
            component: () => import('@/views/product/productAttributes/productBrand'),
            name: 'productBrand',
            meta: { title: '商品品牌', icon: 'el-icon-s-operation' }
          },
          {
            path: 'productSpec',
            component: () => import('@/views/product/productAttributes/productSpec.vue'),
            name: 'productSpec',
            meta: { title: '商品规格', icon: 'el-icon-s-operation' }
          }
        ]
      }
    ]
  },
  {
    path: '/member',
    component: Layout,
    name: 'member',
    meta: {
      title: '会员',
      icon: 'el-icon-s-custom'
    },
    children: [
      {
        path: 'memberManager',
        component: () => import('@/views/member/memberManager'),
        name: 'memberManager',
        meta: { title: '会员管理', icon: 'el-icon-s-check' },
        children: [
          {
            path: 'memberList',
            component: () => import('@/views/member/memberManager/memberList/memberList.vue'),
            name: 'memberList',
            meta: { title: '会员列表', icon: 'el-icon-s-operation' }
          },
          {
            path: 'delMemberList',
            component: () => import('@/views/product/productManager/productReviewList.vue'),
            name: 'delMemberList',
            meta: { title: '回收站', icon: 'el-icon-s-operation' }
          },
          {
            path: 'memberDetails',
            component: () => import('@/views/member/memberManager/memberList/memberDetails.vue'),
            name: 'memberDetails',
            hidden: 1,
            meta: { title: '会员详情', icon: 'el-icon-menu' }
          }
        ]
      },
      {
        path: 'evaluateManager',
        component: () => import('@/views/product/productAttributes'),
        name: 'evaluateManager',
        meta: { title: '评价', icon: 'el-icon-chat-dot-square' },
        children: [
          {
            path: 'evaluateList',
            component: () => import('@/views/product/productAttributes/productClass'),
            name: 'evaluateList',
            meta: { title: '会员评价', icon: 'el-icon-s-operation' }
          },
          {
            path: 'delEvaluateList',
            component: () => import('@/views/product/productManager/productReviewList.vue'),
            name: 'delEvaluateList',
            meta: { title: '回收站', icon: 'el-icon-s-operation' }
          }
        ]
      },
      {
        path: 'integral',
        component: () => import('@/views/product/productAttributes'),
        name: 'integral',
        meta: { title: '积分', icon: 'el-icon-coin' },
        children: [
          {
            path: 'integralHistory',
            component: () => import('@/views/product/productAttributes/productClass'),
            name: 'integralHistory',
            meta: {
              title: '积分历史', icon: 'el-icon-s-operation'
            }
          },
          {
            path: 'userIntegral',
            component: () => import('@/views/product/productManager/productReviewList.vue'),
            name: 'userIntegral',
            meta: { title: '会员积分', icon: 'el-icon-s-operation' }
          }
        ]
      }
    ]
  },
  {
    path: '/store',
    component: Layout,
    name: 'store',
    meta: {
      title: '门店',
      icon: 'el-icon-s-shop'
    },
    children: [
      {
        path: 'storeManager',
        component: () => import('@/views/store/storeManager'),
        name: 'storeManager',
        meta: { title: '门店管理', icon: 'el-icon-shopping-bag-1' },
        children: [
          {
            path: 'storeList',
            component: () => import('@/views/store/storeManager/storeList.vue'),
            name: 'storeList',
            meta: { title: '门店列表', icon: 'el-icon-s-operation' }
          },
          {
            path: 'storeUser',
            component: () => import('@/views/product/productManager/productReviewList.vue'),
            name: 'storeUser',
            meta: { title: '门店导购', icon: 'el-icon-s-operation' }
          }
        ]
      },
      {
        path: 'storeSettlement',
        component: () => import('@/views/product/productManager'),
        name: 'storeSettlement',
        meta: { title: '门店结算', icon: 'el-icon-shopping-bag-1' },
        children: [
          {
            path: 'settlement',
            component: () => import('@/views/product/productManager/productList.vue'),
            name: 'settlement',
            meta: { title: '店铺结算', icon: 'el-icon-s-operation' }
          },
          {
            path: 'reconciliation',
            component: () => import('@/views/product/productManager/productReviewList.vue'),
            name: 'reconciliation',
            meta: { title: '店铺对账', icon: 'el-icon-s-operation' }
          }
        ]
      }
    ]
  },
  {
    path: '/order',
    component: Layout,
    name: 'order',
    meta: {
      title: '订单',
      icon: 'el-icon-s-order'
    },
    children: [
      {
        path: 'orderManager',
        component: () => import('@/views/order/orderManager'),
        name: 'orderManager',
        meta: { title: '订单管理', icon: 'el-icon-files' },
        children: [
          {
            path: 'orderList',
            component: () => import('@/views/order/orderManager/orderList.vue'),
            name: 'orderList',
            meta: { title: '订单列表', icon: 'el-icon-s-operation' }
          },
          {
            path: 'walletOrderList',
            component: () => import('@/views/order/orderManager/walletOrderList.vue'),
            name: 'walletOrderList',
            meta: { title: '充值订单', icon: 'el-icon-s-operation' }
          },
          {
            path: 'orderDetails',
            component: () => import('@/views/order/orderManager/orderDetails.vue'),
            name: 'orderDetails',
            hidden: 1,
            meta: { title: '订单详情', icon: 'el-icon-menu' }
          }
        ]
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
