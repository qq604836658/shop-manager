import request from '@/utils/request'

export const list = (pageParam) =>
  request({
    url: '/member/list',
    method: 'post',
    data: pageParam

  })

export const selectMember = (memberId) =>
  request({
    url: '/member/select/' + memberId,
    method: 'get'
  })

export const saveWallet = (data) =>
  request({
    url: '/memberWallet/saveWallet',
    method: 'post',
    data: data
  })
export const selectOrder = (pageParam) =>
  request({
    url: '/order/selectList',
    method: 'post',
    data: pageParam
  })

export const selectWallet = (pageParam) =>
  request({
    url: '/memberWallet/selectList',
    method: 'post',
    data: pageParam
  })

export const selectIntegral = (pageParam) =>
  request({
    url: '/memberIntegral/selectList',
    method: 'post',
    data: pageParam
  })

export const selectAddress = (pageParam) =>
  request({
    url: '/memberAddress/selectList',
    method: 'post',
    data: pageParam
  })
export const selectAddressById = (addressId) =>
  request({
    url: '/memberAddress/selectById/' + addressId,
    method: 'get'
  })
export const saveOrUpdateAddress = (data) =>
  request({
    url: '/memberAddress/saveOrUpdate',
    method: 'post',
    data: data
  })

export const selectBalance = (memberId) =>
  request({
    url: '/member/select/' + memberId,
    method: 'get'
  })

export const selectShopCart = (memberId) =>
  request({
    url: '/member/select/' + memberId,
    method: 'get'
  })

export const selectEvaluate = (memberId) =>
  request({
    url: '/member/select/' + memberId,
    method: 'get'
  })

export const selectCollection = (memberId) =>
  request({
    url: '/member/select/' + memberId,
    method: 'get'
  })

