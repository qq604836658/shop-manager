import request from '@/utils/request'

export const selectCategoryList = () =>
  request({
    url: '/productCategory/selectList',
    method: 'get'
  })

export const selectBrandList = () =>
  request({
    url: '/productBrand/selectList',
    method: 'get'
  })

export const selectSeasonList = () =>
  request({
    url: '/productSeason/selectList',
    method: 'get'
  })

export const selectYearList = () =>
  request({
    url: '/productYear/selectList',
    method: 'get'
  })

export const selectSpecList = (productId) =>
  request({
    url: '/productSpec/selectList?productId=' + productId,
    method: 'get'
  })

