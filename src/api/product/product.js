import request from '@/utils/request'

export const selectPage = (pageParam) =>
  request({
    url: '/product/selectPage',
    method: 'post',
    data: pageParam

  })

export const selectProduct = (id) =>
  request({
    url: '/product/selectProduct/' + id,
    method: 'get'
  })

export const updateUpDown = (data) =>
  request({
    url: '/product/updateUpDown',
    method: 'post',
    data: data
  })

export const reviewProduct = (data) =>
  request({
    url: '/product/reviewProduct',
    method: 'post',
    data: data
  })

