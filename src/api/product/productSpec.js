import request from '@/utils/request'

export const selectList = () =>
  request({
    url: '/productSpec/selectList',
    method: 'get'
  })

export const saveOrUpdate = (data) =>
  request({
    url: '/productSpec/saveOrUpdate',
    method: 'post',
    data: data
  })
