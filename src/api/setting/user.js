import request from '@/utils/request'

export const list = (pageParam) =>
  request({
    url: '/user/list',
    method: 'post',
    data: pageParam

  })

export const updateStatus = (id, status) =>
  request({
    url: '/user/updateStatus',
    method: 'post',
    data: {
      id: id,
      status: status
    }
  })

export const select = (id) =>
  request({
    url: '/user/select/' + id,
    method: 'get'
  })

export const saveOrUpdate = (user) =>
  request({
    url: '/user/saveOrUpdate',
    method: 'post',
    data: user

  })

export const remove = (id) =>
  request({
    url: '/user/delete/' + id,
    method: 'get'
  })

export const selectRoleList = () =>
  request({
    url: '/role/selectList',
    method: 'post'
  })

