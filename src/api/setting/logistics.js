import request from '@/utils/request'

export const list = (pageParam) =>
  request({
    url: '/logistics/list',
    method: 'post',
    data: pageParam

  })

export const updateStatus = (id, status) =>
  request({
    url: '/logistics/updateStatus',
    method: 'post',
    data: {
      id: id,
      status: status
    }
  })

export const select = (id) =>
  request({
    url: '/logistics/select/' + id,
    method: 'get'
  })

export const saveOrUpdate = (user) =>
  request({
    url: '/logistics/saveOrUpdate',
    method: 'post',
    data: user

  })

export const remove = (id) =>
  request({
    url: '/logistics/delete/' + id,
    method: 'get'
  })
