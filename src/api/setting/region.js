import request from '@/utils/request'

export const selectRegion = (parentId) =>
  request({
    url: '/region/select?parentId=' + parentId,
    method: 'get'
  })

export const selectList = () =>
  request({
    url: '/region/selectList',
    method: 'get'
  })
