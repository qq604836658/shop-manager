import request from '@/utils/request'

export const list = () =>
  request({
    url: '/menu/list',
    method: 'post'
  })

export const remove = (id) =>
  request({
    url: '/menu/delete/' + id,
    method: 'get'
  })

export const select = (id) =>
  request({
    url: '/menu/select/' + id,
    method: 'get'
  })

export const saveOrUpdate = (data) =>
  request({
    url: '/menu/saveOrUpdate',
    method: 'post',
    data: data
  })

