import request from '@/utils/request'

export const list = (pageParam) =>
  request({
    url: '/role/list',
    method: 'post',
    data: pageParam

  })

export const select = (id) =>
  request({
    url: '/role/select/' + id,
    method: 'get'
  })

export const saveOrUpdate = (role) =>
  request({
    url: '/role/saveOrUpdate',
    method: 'post',
    data: role

  })

export const remove = (id) =>
  request({
    url: '/role/delete/' + id,
    method: 'get'
  })

export const selectMenuList = (id) =>
  request({
    url: '/role/selectMenu/' + id,
    method: 'get'
  })

export const saveRoleMenu = (data) =>
  request({
    url: '/role/saveRoleMenu',
    method: 'post',
    data: data
  })

