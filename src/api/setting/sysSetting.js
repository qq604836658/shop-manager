import request from '@/utils/request'

export const selectBySettingCode = (settingCode) =>
  request({
    url: '/sysSetting/selectBySettingCode/' + settingCode,
    method: 'get'
  })

export const saveOrUpdate = (data) =>
  request({
    url: '/sysSetting/saveOrUpdate',
    method: 'post',
    data: data
  })

export const selectList = () =>
  request({
    url: '/sysSetting/selectList',
    method: 'get'
  })

