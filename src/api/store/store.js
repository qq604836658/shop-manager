import request from '@/utils/request'

export const selectPage = (pageParam) =>
  request({
    url: '/store/selectPage',
    method: 'post',
    data: pageParam

  })

export const selectListOpen = () =>
  request({
    url: '/store/selectListOpen',
    method: 'get'

  })

