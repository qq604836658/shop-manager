import request from '@/utils/request'

export const selectList = (pageParam) =>
  request({
    url: '/order/selectList',
    method: 'post',
    data: pageParam

  })

export const selectOrder = (orderCode) =>
  request({
    url: '/order/selectOrder?orderCode=' + orderCode,
    method: 'get'
  })

export const selectOrderLogs = (orderCode) =>
  request({
    url: '/order/selectOrderLogs?orderCode=' + orderCode,
    method: 'get'
  })
export const saveAddress = (data) =>
  request({
    url: '/order/saveAddress',
    method: 'post',
    data: data
  })

export const updatePayOrder = (orderCode) =>
  request({
    url: '/order/payOrder?orderCode=' + orderCode,
    method: 'get'
  })

export const savePayPrice = (data) =>
  request({
    url: '/order/savePayPrice',
    method: 'post',
    data: data
  })

