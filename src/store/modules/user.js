import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    // 获取用户名
    name: '',
    // 获取头像
    avatar: ''
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_USER: (state, user) => {
    state.avatar = user.loginUser.image
    state.name = user.loginUser.userName
    // state.routes = user.routes
  }
}

const actions = {
  // user login
  async login({ commit }, userInfo) {
    const { username, password } = userInfo
    await login({ username: username.trim(), password: password }).then(result => {
      if (result.code === 200) {
        const { data } = result
        commit('SET_TOKEN', data.accessToken)
        setToken(data.accessToken)
        return true
      } else {
        return Promise.reject(new Error(result.msg))
      }
    })
  },

  // get user info
  async getInfo({ commit, state }) {
    await getInfo().then(result => {
      if (result.code === 200) {
        commit('SET_USER', result.data)
        // commit('SET_ROUTES', computeRoutes(asyncRoutes, result.data.routes))
        return true
      } else {
        return Promise.reject(new Error(result.msg))
      }
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token)
        .then(() => {
          removeToken() // must remove  token  first
          resetRouter()
          commit('RESET_STATE')
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise((resolve) => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
